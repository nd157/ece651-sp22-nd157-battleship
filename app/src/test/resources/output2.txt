Player A where do you want to place a Destroyer?
That placement is invalid: the ship goes off the right of the board.
Player A where do you want to place a Destroyer?
---------------------------------------------------------------------------
  0|1|2|3|4|5|6|7|8|9
A  | | | |d| | | | |  A
B  | | | |d| | | | |  B
C  | | | |d| | | | |  C
D  | | | | | | | | |  D
E  | | | | | | | | |  E
F  | | | | | | | | |  F
G  | | | | | | | | |  G
H  | | | | | | | | |  H
I  | | | | | | | | |  I
J  | | | | | | | | |  J
K  | | | | | | | | |  K
L  | | | | | | | | |  L
M  | | | | | | | | |  M
N  | | | | | | | | |  N
O  | | | | | | | | |  O
P  | | | | | | | | |  P
Q  | | | | | | | | |  Q
R  | | | | | | | | |  R
S  | | | | | | | | |  S
T  | | | | | | | | |  T
  0|1|2|3|4|5|6|7|8|9
---------------------------------------------------------------------------
